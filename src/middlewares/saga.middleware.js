"use strict";

const _ = require("lodash");
const kleur = require("kleur");

/**
 * Implementation of SAGA pattern
 */
module.exports = function() {
	return {
		localAction(next, action) {
			if (action.saga) {
				const opts = action.saga;
				// Wrap the handler
				return function sagaHandler(ctx) {
					// Initialize saga compensations
					if (!ctx.meta.saga) {
						ctx.meta = _.assign({}, ctx.meta, { saga: { compensations: [] } });
					}
					// Add localAction compensation into compensations list
					if (opts.compensation) {
						const comp = {
							action: opts.compensation.action,
							params: null,
						};
						ctx.meta.saga.compensations.unshift(comp);
					}
					// Chain this localAction into next action
					return next(ctx)
						.then(res => {
							// ToDo adding handler result values into compensation params (i.e. into comp{ params: ... })
							// if(opts.compensation.params) {
							// 	let comp = _.get(ctx.meta.saga, `compensations.action.${opts.compensation.action}`);
							// 	comp.params = opts.compensation.params.reduce((a, b) => {
							// 		_.set(a, b, _.get(res, b));
							// 		return a;
							// 	}, {});
							// }
							// Return the original result
							return res;
						})
						.catch(err => {
							if (!_.isEmpty(action.saga)) {
								// Start compensating
								ctx.service.logger.warn(kleur.red().bold("SAGA: Some error occured. Start compensating..."));
								ctx.service.logger.info(ctx.meta.saga.compensations);
								if (ctx.meta.saga && Array.isArray(ctx.meta.saga.compensations)) {
									return Promise.mapSeries(ctx.meta.saga.compensations, item => {
										return ctx.call(item.action, item.params);
									}).then(() => {
										throw err;
									});
								}
							}
							throw err;
						});
				};
			}
			// If the feature is disabled we don't wrap it, return the original handler
			// So it won't cut down the performance when the feature is disabled.
			return next;
		}
	};
};