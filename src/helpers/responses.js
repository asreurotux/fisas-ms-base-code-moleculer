/**
 * Transform the results to return the response formated
 * @methods
 * @param {Array|Object} data
 * @param {Object} 	links
 * @param {Array|Object} errors
 * @returns {Object}
 */
function transformToSuccessResponse(data, links, errors) {
	if (!links) {
		links = {};
	}

	if (!data) {
		data = [];
	} else if (!Array.isArray(data)) {
		data = [data];
	}

	if (!errors) {
		errors = [];
	} else if (!Array.isArray(errors)) {
		errors = [errors];
	}

	return Promise.resolve({
		status: "success",
		links,
		data,
		errors
	});
}

module.exports = {
	transformToSuccessResponse
};
