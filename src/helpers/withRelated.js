const _ = require("lodash");

function getSubWithRelated(ctx, field) {
	const withRelateds = ctx.params.withRelated;
	if (Array.isArray(withRelateds)) {
		const withRelatedField = withRelateds.filter(k => k.startsWith(field.concat("."))).map(k => k.split(".").pop());
		return withRelatedField;
	} else {
		return [];
	}
}

function hasOne(docs, ctx, serviceName, newField, identificatorField, searchField="id") {
	const ids = _.uniq(docs.map(d => d[identificatorField]));
	const query = {};
	if(searchField) {
		query[searchField] = ids;
	} else {
		query["id"] = ids;
	}
	return ctx.call(serviceName + ".find", {
		query,
		withRelated: getSubWithRelated(ctx, newField)
	}).then(result => {
		return Promise.resolve(docs.map(d => d[newField] = result.find(c => c ? (c.id === d[identificatorField]) : false) || null));
	});
}

function hasMany(docs, ctx, serviceName, newField, identificatorField = "id", searchField) {
	const ids = docs.map(d => d[identificatorField]);
	return Promise.all(
		ids.map(id => {
			const query = {};
			query[searchField] = id;
			return ctx.call(serviceName + ".find", {
				query
			});
		}).map(p => p.catch(() => []))
	).then(result => {
		return Promise.resolve(docs.map((d, index) => d[newField] = result[index]));
	});
}

module.exports = {
	hasOne,
	hasMany
};