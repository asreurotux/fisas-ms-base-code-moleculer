const Errors = require("./errors");
const Responses = require("./responses");
const WithRelateds = require("./withRelated");

module.exports = {
	Errors,
	Responses,
	WithRelateds
};