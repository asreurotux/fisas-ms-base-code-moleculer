# [1.1.0](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//compare/v1.0.0...v1.1.0) (2021-02-05)


### Features

* **version:** bumped ([d98f524](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/d98f5248b23c9ecfa4502e6dde0b91df5c403075))

# [1.0.0](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//compare/v0.7.0...v1.0.0) (2021-02-05)


### Bug Fixes

* **ci:** local test ([ba38101](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/ba381019872b0dc84d442f920ed8f37c7007d63d))
* **ci:** removed build tag ([3b4b0f3](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/3b4b0f36297c8ea66769ff1eb967a50c78436884))
* **ci:** removed build tag ([b90be83](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/b90be8331bb47b025d7eb16e2a46701716a25797))
* **ci:** removed gitlab template ([fb2ec3c](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/fb2ec3c2ace7ca4344fe68e1f3141e0e646659f2))
* **general:** updated version ([1de4d17](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/1de4d17bb0ee4a1115cf0f280b686a15a6c91841))
* **gitlab:** changed repo url ([5d24c18](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/5d24c18765d5c44e7b8c5a13802e4486f820d0a8))
* **js:** new patch version ([07f4c29](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/07f4c29aecabcb67cf0b819e933fb7f6bee90d56))


### Features

* **ci:** bumped version ([f80d510](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/f80d510392cc019122dc82d32b41c01513c0ac59))
* **ci:** bumping minor version ([d9dd9d9](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/d9dd9d991b13215b326840fca206ec527aa0ff2d))
* **ci:** local test ([5778ab8](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/5778ab8e366c0cffde70c9adf7efc937729c6e8d))
* **ci:** minor version ([eda0f01](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/eda0f0100040fd5a73e3c77dfca10c1216e9cd12))
* **ci:** small change ([472775d](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/472775de73abf1360df60ba02eaef9c7f4196bf2))
* **js:** new minor version ([20813d8](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/20813d8c0bc51a98f6a081ebdd85ddf823045d35))
* **version:** bumped ([9f2da07](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/9f2da07b43a73ae3bdb122fdaa2a119196b2d460))


### Performance Improvements

* **git:** changed dependencies ([73e6fb0](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/73e6fb0730bf29f3662c9d8a7dba04eeeb85aa34))
* **git:** next major release ([cc56ea4](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/cc56ea4fa63158c038d9dcf2f20ec7d0deea5f14))


### BREAKING CHANGES

* **git:** Example change to trigger a major

# [1.0.0-rc.6](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//compare/v1.0.0-rc.5...v1.0.0-rc.6) (2021-02-05)


### Bug Fixes

* **ci:** local test ([ba38101](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/ba381019872b0dc84d442f920ed8f37c7007d63d))


### Features

* **ci:** local test ([5778ab8](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/5778ab8e366c0cffde70c9adf7efc937729c6e8d))

# [1.0.0-rc.5](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//compare/v1.0.0-rc.4...v1.0.0-rc.5) (2021-02-05)


### Features

* **ci:** bumped version ([f80d510](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/f80d510392cc019122dc82d32b41c01513c0ac59))

# [1.0.0-rc.4](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//compare/v1.0.0-rc.3...v1.0.0-rc.4) (2021-02-05)


### Bug Fixes

* **js:** new patch version ([07f4c29](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/07f4c29aecabcb67cf0b819e933fb7f6bee90d56))


### Features

* **ci:** minor version ([eda0f01](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/eda0f0100040fd5a73e3c77dfca10c1216e9cd12))
* **ci:** small change ([472775d](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/472775de73abf1360df60ba02eaef9c7f4196bf2))
* **js:** new minor version ([20813d8](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/20813d8c0bc51a98f6a081ebdd85ddf823045d35))

# [1.0.0](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//compare/v0.7.0...v1.0.0) (2021-02-05)


### Bug Fixes

* **ci:** removed build tag ([3b4b0f3](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/3b4b0f36297c8ea66769ff1eb967a50c78436884))
* **ci:** removed build tag ([b90be83](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/b90be8331bb47b025d7eb16e2a46701716a25797))
* **ci:** removed gitlab template ([fb2ec3c](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/fb2ec3c2ace7ca4344fe68e1f3141e0e646659f2))
* **general:** updated version ([1de4d17](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/1de4d17bb0ee4a1115cf0f280b686a15a6c91841))
* **gitlab:** changed repo url ([5d24c18](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/5d24c18765d5c44e7b8c5a13802e4486f820d0a8))
* **js:** new patch version ([07f4c29](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/07f4c29aecabcb67cf0b819e933fb7f6bee90d56))


### Features

* **ci:** bumping minor version ([d9dd9d9](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/d9dd9d991b13215b326840fca206ec527aa0ff2d))
* **js:** new minor version ([20813d8](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/20813d8c0bc51a98f6a081ebdd85ddf823045d35))


### Performance Improvements

* **git:** changed dependencies ([73e6fb0](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/73e6fb0730bf29f3662c9d8a7dba04eeeb85aa34))
* **git:** next major release ([cc56ea4](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/cc56ea4fa63158c038d9dcf2f20ec7d0deea5f14))


### BREAKING CHANGES

* **git:** Example change to trigger a major

# [1.0.0-rc.3](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//compare/v1.0.0-rc.2...v1.0.0-rc.3) (2021-02-05)


### Bug Fixes

* **ci:** removed build tag ([3b4b0f3](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/3b4b0f36297c8ea66769ff1eb967a50c78436884))
* **ci:** removed build tag ([b90be83](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/b90be8331bb47b025d7eb16e2a46701716a25797))
* **ci:** removed gitlab template ([fb2ec3c](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/fb2ec3c2ace7ca4344fe68e1f3141e0e646659f2))

# [1.0.0-rc.2](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//compare/v1.0.0-rc.1...v1.0.0-rc.2) (2021-02-05)


### Features

* **ci:** bumping minor version ([d9dd9d9](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/d9dd9d991b13215b326840fca206ec527aa0ff2d))

# [1.0.0-rc.1](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//compare/v0.7.1-rc.2...v1.0.0-rc.1) (2021-02-05)


### Performance Improvements

* **git:** changed dependencies ([73e6fb0](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/73e6fb0730bf29f3662c9d8a7dba04eeeb85aa34))


### BREAKING CHANGES

* **git:** Example change to trigger a major

## [0.7.1-rc.2](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//compare/v0.7.1-rc.1...v0.7.1-rc.2) (2021-02-05)


### Performance Improvements

* **git:** next major release ([cc56ea4](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/cc56ea4fa63158c038d9dcf2f20ec7d0deea5f14))

## [0.7.1-rc.1](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//compare/v0.7.0...v0.7.1-rc.1) (2021-02-05)


### Bug Fixes

* **general:** updated version ([1de4d17](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/1de4d17bb0ee4a1115cf0f280b686a15a6c91841))
* **gitlab:** changed repo url ([5d24c18](https://gitlab.com/asreurotux/fisas-ms-base-code-moleculer//commit/5d24c18765d5c44e7b8c5a13802e4486f820d0a8))

# [0.7.0](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.6.0...v0.7.0) (2021-01-29)


### Features

* **knex:** add new query operators ([a185511](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/a185511bda6903f87a794bf6d8a52b924f463f85))

# [0.7.0-rc.1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.6.0...v0.7.0-rc.1) (2021-01-29)


### Features

* **knex:** add new query operators ([a185511](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/a185511bda6903f87a794bf6d8a52b924f463f85))

# [0.6.0](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.6...v0.6.0) (2021-01-26)


### Features

* add parser to PG ([db39d03](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/db39d03943a3b565e41c99853748bfe9bc6fd4b8))
* release new version ([3079a01](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/3079a016f3e68dece96cd849b180a0792b306413))

# [0.6.0-rc.1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.6...v0.6.0-rc.1) (2021-01-26)


### Features

* add parser to PG ([db39d03](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/db39d03943a3b565e41c99853748bfe9bc6fd4b8))

## [0.5.6](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.5...v0.5.6) (2021-01-21)


### Bug Fixes

* **dbMixin:** parse shorthand validator on patch ([c3eedc2](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/c3eedc2e8b8f686306d8e815b7d200e41fa2a0b8))

## [0.5.6-rc.1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.5...v0.5.6-rc.1) (2021-01-21)


### Bug Fixes

* **dbMixin:** parse shorthand validator on patch ([c3eedc2](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/c3eedc2e8b8f686306d8e815b7d200e41fa2a0b8))

## [0.5.5](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.4...v0.5.5) (2021-01-19)


### Bug Fixes

* **knex:** is date valid format ([d9336d4](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/d9336d4ef01a254620e29ea01bac6c08a33e8a3f))

## [0.5.4](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.3...v0.5.4) (2021-01-18)


### Bug Fixes

* **db.mixin:** count take searchFileds in query ([1e3e38e](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/1e3e38ea3fc0189c00731e0d6c0222e27acdd853))
* **knex:** add ::date conversion on search for data ([1d1dc3f](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/1d1dc3ffc272c166cddcc96e439adf20a9e62be7))
* **knex:** fix date field convertion ([f35b2d7](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/f35b2d7adc7d763a85ac348ccf368074aeb8ff52))
* **knex:** if is date convert on postgres ([8aa28e7](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/8aa28e7d6c0712647017e9c55f78559bee4ffd48))
* add date validation and full text search is on and condition ([b5dff8e](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/b5dff8edc541129654fd874fa3cbf3ec4af9f61a))

## [0.5.3-rc.5](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.3-rc.4...v0.5.3-rc.5) (2021-01-18)


### Bug Fixes

* **knex:** fix date field convertion ([f35b2d7](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/f35b2d7adc7d763a85ac348ccf368074aeb8ff52))

## [0.5.3-rc.4](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.3-rc.3...v0.5.3-rc.4) (2021-01-18)


### Bug Fixes

* **knex:** if is date convert on postgres ([8aa28e7](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/8aa28e7d6c0712647017e9c55f78559bee4ffd48))

## [0.5.3-rc.3](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.3-rc.2...v0.5.3-rc.3) (2021-01-13)


### Bug Fixes

* **knex:** add ::date conversion on search for data ([1d1dc3f](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/1d1dc3ffc272c166cddcc96e439adf20a9e62be7))

## [0.5.3-rc.2](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.3-rc.1...v0.5.3-rc.2) (2021-01-13)


### Bug Fixes

* **db.mixin:** count take searchFileds in query ([1e3e38e](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/1e3e38ea3fc0189c00731e0d6c0222e27acdd853))

## [0.5.3-rc.1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.2...v0.5.3-rc.1) (2021-01-13)


### Bug Fixes

* add date validation and full text search is on and condition ([b5dff8e](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/b5dff8edc541129654fd874fa3cbf3ec4af9f61a))
* **knex:** search is now INSENSATIVE CASE ([2085783](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/20857836b22dedd57512ee6c9679e99cacedec3b))

## [0.5.2](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.1...v0.5.2) (2021-01-06)
* **helpers:** remove this in getSubWithRelated ([4bd918b](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/4bd918b37ef2dcfd92a0bfbb2ffd8253a6ff4d5c))
## [0.5.1-rc.1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.0...v0.5.1-rc.1) (2021-01-13)

### Bug Fixes
* add date validation and full text search is on and condition ([b5dff8e](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/b5dff8edc541129654fd874fa3cbf3ec4af9f61a))
* **knex:** search is now INSENSATIVE CASE ([2085783](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/20857836b22dedd57512ee6c9679e99cacedec3b))
## [0.5.1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.5.0...v0.5.1) (2021-01-06)

### Bug Fixes

* **helpers:** add searchField to hasOne ([7f3fb10](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/7f3fb10e02d1482f7d54a3ff5adb790159f5512e))


# [0.5.0](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.4.6...v0.5.0) (2020-12-02)


### Features

* **erros:** add forbidden and unauthorized errors ([181722b](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/181722b80751103b0c1404619d0bca7f68e828b9))

## [0.4.6](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.4.5...v0.4.6) (2020-11-27)


### Bug Fixes

* **db-mixin:** if withRelated = false skip step ([bba1a21](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/bba1a2113b3367d0f4eb18afdfb8c3afbf4208a0))

## [0.4.5](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.4.4...v0.4.5) (2020-11-27)


### Bug Fixes

* **db.mixin:** add withRelated boolean validation ([f3f7c3a](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/f3f7c3a22b28cf6930c28af0cab3f9b42a35349a))

## [0.4.4](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.4.3...v0.4.4) (2020-11-18)


### Bug Fixes

* **db.mixin:** metadatos in the response to save in cache ([0fec57b](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/0fec57b89d44a87a808867c6a376e6df8885316d))

## [0.4.3](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.4.2...v0.4.3) (2020-11-02)


### Bug Fixes

* **db.mixin:** fix _insert on enteties empty array ([116c469](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/116c469d7daf37dd841d609bc510a214202559b9))

## [0.4.2](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.4.1...v0.4.2) (2020-11-02)


### Bug Fixes

* **db.mixin:** fix insert return value ([30da099](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/30da099e2d7ff1210b9efafd00ecdf02d23e729e))

## [0.4.1](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.4.0...v0.4.1) (2020-10-30)


### Bug Fixes

* **db.mixin:** fix limit sanitize in case of -1 ([47af10e](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/47af10efd55757480c3b03b5638ab51ea55cd451))

# [0.4.0](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.3.0...v0.4.0) (2020-10-30)


### Features

* **validator:** add validateExternalIds ([05f6c07](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/05f6c07ed18ce804f474e838c097e009916c182f))

# [0.3.0](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/compare/v0.2.6...v0.3.0) (2020-10-30)


### Features

* **db.mixin:** add function for raw queries ([ad31c4e](https://gitlab.com/fi-sas/fisas-ms-base-code-moleculer/commit/ad31c4ebce8e81ae999fe757a328e7f01c077c18))

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.2.6](///compare/v0.2.5...v0.2.6) (2020-10-27)

### [0.2.5](///compare/v0.2.4...v0.2.5) (2020-10-26)


### Bug Fixes

* **db.mixin:** fix _insert function no return ([1150285](///commit/1150285d4737a4ec386caf188c82d0d930b08292))

### [0.2.4](///compare/v0.2.3...v0.2.4) (2020-10-22)


### Bug Fixes

* **knex:** increment knex version to allow set seeds directory path as array ([3c3ba3d](///commit/3c3ba3d1c90f150cfde6d7ae4f33bb8da9b539e6))

### [0.2.3](///compare/v0.2.2...v0.2.3) (2020-10-06)


### Features

* saga pattern middleware ([4bd4081](///commit/4bd4081263424e29da70170f72fbc74756075532))

### [0.2.2](///compare/v0.2.0...v0.2.2) (2020-09-28)


### Bug Fixes

* **db.adapter:** remove params from count ([387eebc](///commit/387eebc379fad1181ec24f0c05fe3cfdf2d63bda))
* **helpers:** fix withRelated hasOne helper ([e4c8894](///commit/e4c889462e360e6c782463d20720f79b71589315))
* **knex-adapter:** fix the insert returning for postgres ([d21321c](///commit/d21321cd8a3ac43ff387737b7eb332324a74ab69))

### [0.2.1](///compare/v0.2.0...v0.2.1) (2020-09-28)


### Bug Fixes

* **helpers:** fix withRelated hasOne helper ([e4c8894](///commit/e4c889462e360e6c782463d20720f79b71589315))
* **knex-adapter:** fix the insert returning for postgres ([d21321c](///commit/d21321cd8a3ac43ff387737b7eb332324a74ab69))

## [0.2.0](///compare/v0.1.17...v0.2.0) (2020-07-01)


### Features

* **db:** broadcast a event automatically ([778d00f](///commit/778d00f003abd740fa138b277080efa420562d90))

### [0.1.17](///compare/v0.1.16...v0.1.17) (2020-07-01)


### Bug Fixes

* **db:** add withRelateds to authorized fields ([0ae8e88](///commit/0ae8e88ca31774e661d76afaefa0ce02a3bcfa51))

### [0.1.16](///compare/v0.1.15...v0.1.16) (2020-06-30)


### Bug Fixes

* **db:** add withRelateds to authorized fields ([dc781c9](///commit/dc781c9613030b5e1004fdb93d0978bf994b461b))
* **db:** fix update return value ([384a4df](///commit/384a4df8cda1b6be16c1ca079aabaa92453b7277))

### [0.1.15](///compare/v0.1.14...v0.1.15) (2020-06-30)


### Bug Fixes

* **db:** fix path and foriegn fields removal ([2bf1895](///commit/2bf18958949bfb46e3eac31183e024ab2b67a80f))

### [0.1.14](///compare/v0.1.13...v0.1.14) (2020-06-30)


### Bug Fixes

* **db:** add remove fireign fields to update / patch ([2e824cd](///commit/2e824cdd2051d5f12d00fed5012da23cca5aadc5))

### [0.1.13](///compare/v0.1.12...v0.1.13) (2020-06-30)


### Bug Fixes

* **db:** fix patch entity validator compilation ([89a917f](///commit/89a917fcb2f3477fb2822bf287c33930a249ea21))

### [0.1.12](///compare/v0.1.11...v0.1.12) (2020-06-30)


### Bug Fixes

* **db:** fix patch entity validator ([8b20d87](///commit/8b20d87458f3437a90d7b9c9b5c4dd7a0e0525fb))

### [0.1.11](///compare/v0.1.10...v0.1.11) (2020-06-30)


### Features

* **db:** add defaultWithRelateds ([aba0505](///commit/aba0505de30947b58bada5e13dec69bb27586788))


### Bug Fixes

* **db:** add entity validator to update and patch method ([d34d158](///commit/d34d158017ca92f2a7880bd11345b88e941b9f08))
* **db:** lint fixs ([852b5ba](///commit/852b5ba537cb0bee3984e4d754ad72a2762ea090))

### [0.1.10](///compare/v0.1.9...v0.1.10) (2020-06-26)

### [0.1.9](///compare/v0.1.8...v0.1.9) (2020-06-25)


### Bug Fixes

* **db.mixin:** fix lint warnings ([b61eac7](///commit/b61eac76ddd920346f46598c4932fd860a88b40e))
* **errors:** change validation error ([0abe831](///commit/0abe831db71112e45a8388a6621708523d9380d4))

### [0.1.8](///compare/v0.1.6...v0.1.8) (2020-06-24)


### Features

* implemented openapi.mixin ([ffc44e3](///commit/ffc44e37f8ec078a35973283bbe6909b4b4379f8))


### Bug Fixes

* **errors:** fix not  found error ([02abd1c](///commit/02abd1c6e30f725fee4931c516649e2f45263413))

### [0.1.7](///compare/v0.1.6...v0.1.7) (2020-06-17)


### Features

* implemented openapi.mixin ([ffc44e3](///commit/ffc44e37f8ec078a35973283bbe6909b4b4379f8))

### [0.1.6](///compare/v0.1.5...v0.1.6) (2020-05-12)


### Bug Fixes

* **knex.adapter:** fix getById return object ([582b75f](///commit/582b75f398c3acc121c400233fca6e9a647f8123))

### [0.1.5](///compare/v0.1.4...v0.1.5) (2020-05-12)


### Features

* **errors:** add validation error ([028be5f](///commit/028be5f7f2c93f74e9b7c6742ec0b016b1c62734))


### Bug Fixes

* **db.mixin:** fix parseBooleans method ([498cee0](///commit/498cee033e0cb008e09f066502614b4cddb2eb2c))

### [0.1.4](///compare/v0.1.3...v0.1.4) (2020-05-12)


### Bug Fixes

* **knex:** add destroy on disconnect ([d385d79](///commit/d385d796420db1b3e12262ba5f6206860ec6e9f1))

### [0.1.3](///compare/v0.1.2...v0.1.3) (2020-05-12)


### Bug Fixes

* **db.mixin:** remove memory adapter opts ([8ddba40](///commit/8ddba40ede103e235c33c41510ff202f40dcdcd5))

### [0.1.2](///compare/v0.1.1...v0.1.2) (2020-05-11)

### 0.1.1 (2020-05-11)


### Features

* **core:** add initial version ([dfd679b](///commit/dfd679b042f0a85b78661264c41f15ed4c7a27bc))
